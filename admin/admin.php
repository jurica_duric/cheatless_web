	<!DOCTYPE html>
	<html lang="en">
		<head>
		<title>Cheatless licence administration</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	   <script>
	   function sortTableNumeric() {
	  var table, rows, switching, i, x, y, shouldSwitch;
	  table = document.getElementById("tablica");
	  switching = true;
	  /*Make a loop that will continue until
	  no switching has been done:*/
	  while (switching) {
		//start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/*Loop through all table rows (except the
		first, which contains table headers):*/
		for (i = 1; i < (rows.length - 1); i++) {
		  //start by saying there should be no switching:
		  shouldSwitch = false;
		  /*Get the two elements you want to compare,
		  one from current row and one from the next:*/
		  x = rows[i].getElementsByTagName("TD")[0];
		  y = rows[i + 1].getElementsByTagName("TD")[0];
		  //check if the two rows should switch place:
		  if (Number(x.innerHTML) > Number(y.innerHTML)) {
			//if so, mark as a switch and break the loop:
			shouldSwitch = true;
			break;
		  }
		}
		if (shouldSwitch) {
		  /*If a switch has been marked, make the switch
		  and mark that a switch has been done:*/
		  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		  switching = true;
		}
	  }
	}
	function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("tablica");
	  switching = true;
	  //Set the sorting direction to ascending:
	  dir = "asc"; 
	  /*Make a loop that will continue until
	  no switching has been done:*/
	  while (switching) {
		//start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/*Loop through all table rows (except the
		first, which contains table headers):*/
		for (i = 1; i < (rows.length - 1); i++) {
		  //start by saying there should be no switching:
		  shouldSwitch = false;
		  /*Get the two elements you want to compare,
		  one from current row and one from the next:*/
		  x = rows[i].getElementsByTagName("TD")[n];
		  y = rows[i + 1].getElementsByTagName("TD")[n];
		  /*check if the two rows should switch place,
		  based on the direction, asc or desc:*/
		  if (dir == "asc") {
			if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
			  //if so, mark as a switch and break the loop:
			  shouldSwitch= true;
			  break;
			}
		  } else if (dir == "desc") {
			if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
			  //if so, mark as a switch and break the loop:
			  shouldSwitch = true;
			  break;
			}
		  }
		}
		if (shouldSwitch) {
		  /*If a switch has been marked, make the switch
		  and mark that a switch has been done:*/
		  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		  switching = true;
		  //Each time a switch is done, increase this count by 1:
		  switchcount ++;      
		} else {
		  /*If no switching has been done AND the direction is "asc",
		  set the direction to "desc" and run the while loop again.*/
		  if (switchcount == 0 && dir == "asc") {
			dir = "desc";
			switching = true;
		  }
		}
	  }
	}
	</script>

	<script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	  $('#message').delay(5000).fadeOut('slow');
	  
	});
	</script>
	   </head>
		<body>
		
			<div class="container">

			<div class="row justify-content-center">
				<h3>U sustav ste prijavljeni kao:  
					<?php 
					ini_set('display_errors', 1);
					session_start();
					
					require_once 'process.php';
					if(isset($_SESSION['login'])){
						echo $_SESSION['login'];
					}
					/*else{
						header('Location: index.php');
					}*/
					//$df = disk_free_space("/");
					//$ds = disk_total_space("/");
					
    //echo $bytes . '<br />';
    //echo sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '<br />';
					//echo $df;
					
					//echo sprintf('%1.2f' , $ds / pow($base,$class)) . ' ' . $si_prefix[$class] . '<br />';
					function startsWith($haystack, $needle) {
						return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
					}
					$dir = '/../../../../home/cheatless/CheatlessTest';
					$mysqli=new mysqli('localhost', 'root','Cheatless1.', 'Cheatless')or die(mysqli_error($mysqli));
					$result=$mysqli->query("SELECT u.*, ut.userType as type FROM Users u INNER JOIN UserType ut ON u.userType=ut.idUserType") or die($mysqli->error);
							//print_r($result);
					?>
</h3>
			</div>
			<?php
			
				if(isset($_SESSION['message'])){ ?>
				<div id="message" class="alert alert-<?=$_SESSION['msg_type']?>">
					<?php
						echo $_SESSION['message'];
						unset($_SESSION['message']);
					?>
				</div>
				<?php } ?>
		<ul class="nav nav-tabs nav-justified">
		 <li class="nav-item">
				<a href="#activeUsers" class="nav-link active" data-toggle="tab">Active users</a>
			</li>
			<li class="nav-item">
				<a href="#licences" class="nav-link" data-toggle="tab">Licences</a>
			</li>
			<li class="nav-item">
				<a href="#settings" class="nav-link" data-toggle="tab">Settings</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade show" id="licences">			 
				<div class="row justify-content-center">
					<h4 class="mt-2">List of all users and licences</h4>
					<div class="table-responsive-lg">
						<table class="table table-hover table-condensed" id="tablica">
							<thead>
								<tr>
									<th onclick="sortTableNumeric()">Id</th>
									<th onclick="sortTable(1)">E-mail</th>
									<th onclick="sortTable(2)">Datum kreiranja</th>
									<th onclick="sortTable(3)">Datum isteka licence</th>
									<th onclick="sortTable(4)">Licenca</th>
									<th onclick="sortTable(5)">Licencirano</th>
									<th onclick="sortTable(6)">Tip korisnika</th>
									<th colspan="4" style="text-align:center;">Akcija</th>
								</tr>
							</thead>
						<?php
						while($row=$result->fetch_assoc()){
							
						?>
							<tr>
								<td><?php echo $row['idUsers'];?></td>
								<td><?php echo $row['email'];?></td>
								<td><?php echo date("d.m.Y. H:i:s", strtotime($row['creationDate']));?></td>
								<td><?php echo date("d.m.Y. H:i:s", strtotime($row['exparationDate']));?></td>
								<td data-toggle="tooltip" title="<?php echo $row['licence'];?>"><?php echo str_repeat("*", strlen(substr($row['licence'],0,10)));?></td>
								<td><?php echo $row['licenced'];?></td>
								<td><?php echo $row['type'];?></td>
								<td><a href="admin.php?edit=<?php echo $row['idUsers'];?>" class="btn btn-info">Uredi podatke</a></td>
								<!--<td><a href="admin.php?delete=<?php echo $row['idUsers'];?>" class="btn btn-danger">Izbriši podatke</a></td>-->
								<td><a href="process.php?update=<?php echo $row['idUsers'];?>" class="btn btn-success">Kreiraj licencu</a></td>
								<td><a href="process.php?send=<?php echo $row['idUsers'];?>" class="btn btn-success">Pošalji licencu</a></td>
							  </tr>
						<?php
						
						}
						?> 
							<tfoot>
							<td>Ukupno korisnika</td>
							<td colspan="10" style="text-align:center;"><?php echo $result->num_rows; $mysqli->close();?></td>
							</tfoot>
						</table>
					</div>
				</div>
			 </div>
			<div class="tab-pane fade show active" id="activeUsers">
			
				<div class="row justify-content-center">
					<h4 class="mt-2">List of all active users that is currently using Cheatless system</h4>
					<h4>Trenutni broj korisnika koji koriste Cheatless: <?php
							$mysqli=new mysqli('localhost', 'root','Cheatless1.', 'Cheatless')or die(mysqli_error($mysqli));
							$result=$mysqli->query("SELECT ch.*, u.statusName FROM CheatlessApp ch INNER JOIN userStatuses u ON ch.userStatus=u.id WHERE statusName='Files uploaded and connected' OR statusName='Connected' OR statusName='Try of multiple use';") or die($mysqli->error);	
							echo $result->num_rows; $mysqli->close();
					?>
					</h4>
					<?php
						$mysqli=new mysqli('localhost', 'root','Cheatless1.', 'Cheatless')or die(mysqli_error($mysqli));
						$result=$mysqli->query("SELECT ch.*, u.statusName FROM CheatlessApp ch INNER JOIN userStatuses u ON ch.userStatus=u.id ORDER BY lastUpdate DESC;") or die($mysqli->error);
					?>
					<table class="table table-hover table-condensed" id="tablica">
						<thead>
							<tr>
								<th onclick="sortTableNumeric()">Id</th>
								<th onclick="sortTable(1)">E-mail</th>
								<th onclick="sortTable(2)">Javna IP adresa</th>
								<!--<th onclick="sortTable(3)">Lokalna IP adresa</th>-->
								<th onclick="sortTable(4)">Status</th>
								<!--<th onclick="sortTable(5)">Veličina datoteke</th>-->
								<th onclick="sortTable(6)">Datum zadnje aktivnosti</th>
								<th>Odblokiraj</th>
							</tr>
						</thead>
					<?php
					while($row=$result->fetch_assoc()){
						
					?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td><?php echo $row['email'];?></td>
							<td><?php echo $row['publicIp'];?></td>
							
							<td><?php echo $row['statusName'];?></td>
							<!--<td><?php //echo $row['videoSize'];?></td>-->
							<td><?php echo date("d.m.Y. H:i:s", strtotime($row['lastUpdate']));?></td>
							<td><a href="process.php?unblock=<?php echo $row['id'];?>" class="btn btn-success">Odblokiraj prijavu</a></td>
							<td><button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo-<?php echo $row['id'];?>">Detalji</button></td>
						</tr>
						<tr>
							<td colspan=7>
								<span id="demo-<?php echo $row['id'];?>" class="collapse">
								<?php
								chdir($dir);
								array_multisort(array_map('filemtime', ($files = glob("*.{mp4}", GLOB_BRACE))), SORT_DESC, $files);
								foreach($files as $filename)
								{
									if (startsWith($filename,$row['email'])==1){
										//echo substr($filename, 0, -4);
										  //echo getcwd();
										//echo $_SERVER['SERVER_NAME'];
										$path = $_SERVER['SERVER_NAME'] . getcwd() ."/" . $filename;
										//echo getcwd() ."/" . $filename;
										echo $path;
										//echo $filename;?>
										<video width="320" height="240" controls>
										  <!--<source src="<?php echo $path ?>" type="video/mp4">-->
										  <!--<source src="../Cheatless/VsiTe/test/student@vsite.hr_DESKTOP-OE2KCI3_12_03_2021_13_40_31_DEMO.mp4" type="video/mp4">
										  Your browser does not support the video tag.
										</video>-->
										
										<?php break;		
									} 
									/*
									else{
										echo "Ne postoji niti jedan video zapis";
										break;
									}*/
								} 
								?></span>
							</td>
						</tr>
					<?php
					
					}
					?> 
						<tfoot>
						<td colspan="3" style="text-align:center;">Trenutni broj korisnika koji koriste Cheatless</td>
						<td colspan="3" style="text-align:center;">
							<?php
								$result=$mysqli->query("SELECT ch.*, u.statusName FROM CheatlessApp ch INNER JOIN userStatuses u ON ch.userStatus=u.id WHERE statusName='Connected';") or die($mysqli->error);	
								echo $result->num_rows; $mysqli->close();
							?>
						</td>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="tab-pane fade" id="settings">
				<div class="justify-content-center">
					<h4 class="mt-2">Settings tab content</h4>
					<h4>
						<?php
							$ds = disk_total_space("/");
							//$ds = disk_total_space("/home/cheatless/storage");
							$bytes = disk_free_space("/");
							$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' );
							$base = 1024;
							$class = min((int)log($bytes , $base) , count($si_prefix) - 1);
						?>
						<label for="FreeDiskSpace">Free disk space:</label>
						<meter id="FreeDiskSpace" min="0" max="<?php echo $ds;?>"
							low="<?php echo $ds*0.9;?>" high="<?php echo $ds*0.3;?>"
							value="<?php echo $bytes;?>">
						</meter> 
						<?php echo sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . ' of ';?>
						<?php echo sprintf('%1.2f' , $ds / pow($base,$class)) . ' ' . $si_prefix[$class] . '<br />';?>
					</h4>
				</div>
			</div>
		</div>
		</div>
		
		</body>
	</html>
