<!DOCTYPE html>
<?php
session_start();
//$mail=$_POST['email'];
if(!isset($_SESSION['mail'])){
	$mail=$_GET['email'];
	$_SESSION['mail']=$mail;
}
/*if(!isset($_SESSION['mail']){
	header('Location: https://huzp.vsite.hr/webcamtestphp/')
}
else{
	$mail=$_POST['email'];
	$_SESSION['mail']=$mail;
}*/
//echo $mail;
?>
<html>
<head>
    <title>Cheatless web kamera</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
	<style>
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  top: 100px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

/*#results, #my_camera{
	display:none;
	visibility: hidden;
}
.image-tag {
	visibility: hidden;
	display:none;
}*/
@-webkit-keyframes fadein {
  from {top: 0; opacity: 0;} 
  to {top: 100px; opacity: 1;}
}

@keyframes fadein {
  from {top: 0; opacity: 0;}
  to {top: 100px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {top: 100px; opacity: 1;} 
  to {top: 0; opacity: 0;}
}

@keyframes fadeout {
  from {top: 100px; opacity: 1;}
  to {top: 0; opacity: 0;}
}
</style>
</head>
<!--<body onload="sve2()">-->
<body>
  
<div class="container">
    <h1 class="text-center">Cheatless web kamera</h1>
   <div id="snackbar">Slika je spremljena na server!</div>
    <form method="POST" action="storeImage.php" id="form">
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<input type=button class="btn btn-primary btn-lg" value="Prednja kamera" onClick="frontCamera()" id="front">
				<input type=button class="btn btn-primary btn-lg" value="Zadnja kamera" onClick="backCamera()" id="back">
				<input type=button class="btn btn-secondary btn-lg" value="Snimi sliku" onClick="takeSnapshot()" id="snapshot">
				<input type=button class="btn btn-secondary btn-lg" value="Zaustavi kameru" onClick="stopCamera()" id="stop">
				<input type=button class="btn btn-secondary btn-lg" value="sve" onClick="sve2()" id="sve">
                <button class="btn btn-success btn-lg" onclick="uploadImage()" id="save">Spremanje slike</button>
				
			</div>
			<!--<div class="col-md-3 text-center">
			
			</div>
			<div class="col-md-3 text-center">
				
            </div>-->
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
                <div id="my_camera" class="img-responsive"></div>
                <!--<input type=button value="Snimi sliku" onClick="take_snapshot()">-->
                <input type="hidden" name="image" class="image-tag">
            </div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div id="results"></div>
			</div>
		</div>
		
		<!--<div class="row">
            <div class="col-md-12">
                <div id="results"></div>
            </div>
            <!--<div class="col-md-12 text-center">
                <br/>
                <button class="btn btn-success">Upload slike</button>
            </div>-->
    </form>
</div>
  
<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
/*
document.getElementById("stop").disabled = true;
document.getElementById("snapshot").disabled = true;
document.getElementById("save").disabled = true;*/
function frontCamera(){
	
	stopCamera();
	Webcam.set({
		constraints: {
				facingMode: 'user'
			},
        width: 640,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
	Webcam.attach('#my_camera');
	document.getElementById("stop").disabled = false;
	document.getElementById("snapshot").disabled = false;
}

function backCamera(){
	stopCamera();
	Webcam.set({
		constraints: {
				facingMode: 'environment'
			},
        width: 640,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
	Webcam.attach( '#my_camera' );
	document.getElementById("stop").disabled = false;
	document.getElementById("snapshot").disabled = false;
}   
  
function stopCamera(){
	Webcam.reset();    
}
function uploadImage() {
	//takeSnapshot();
	//var x = document.getElementById("snackbar");
	//x.className = "show";
	//setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
	document.getElementById("form").submit();
}
function takeSnapshot() {
	//uploadImage();
	Webcam.snap( function(data_uri) {
	$(".image-tag").val(data_uri);
		document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
			
		} );
		document.getElementById("save").disabled = false;
		Webcam.reset(); 
//document.getElementById("form").submit();
		//Webcam.reset();
		//uploadImage();
    }

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}
function sve2(){
	var min = 5,
    max = 20;
  var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 10
	//var time=getRndInteger(10,30);
	frontCamera();
	//backCamera();
	//setTimeout(frontCamera(), 1000);
	setTimeout(takeSnapshot, rand * 1000);
	//setTimeout(uploadImage, rand * 1000);
	//setTimeout(function(){ takeSnapshot(); }, 10000);
	//setTimeout(function(){ uploadImage(); }, 10000);
	//setTimeout(takeSnapshot(), 5000);
	
	/*takeSnapshot();
	uploadImage();*/
}
</script>
 
</body>
</html>