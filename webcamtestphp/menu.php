<!DOCTYPE html>
<?php
session_start();

if(!isset($_SESSION['mail'])){
	$mail=$_POST['email'];
	$_SESSION['mail']=$mail;
}
/*
else{
	$_SESSION['camera'];
}*/
/*echo $_SESSION['mail'];
echo '<a href="email.php">Test</a>';*/
?>
<html>
<head>
    <title>Cheatless web kamera</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
	<style>
	/*#results, #my_camera{
		display:none;
		visibility: hidden;
	}
	.image-tag {
		visibility: hidden;
		display:none;
	}*/
	</style>
	<script>
	// Check browser support
	var timer;
	var startTimer;
	</script>
</head>
<body onload="record()">
<!--<body>-->
  
<div class="container">


	<h6 class="text-center">Prijavljeni ste kao: <?php echo $_SESSION['mail'];?>
	<br>Trenutno koristite <span id="result"></span> kameru</h6>
    <h3 class="text-center">Odaberite koju kameru želite koristiti za snimanje</h3>
    <form method="POST" action="storeImage.php" id="form">
        <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<input type=button class="btn btn-primary btn-lg" value="Prednja kamera" onClick="frontCamera()" id="front">
				<input type=button class="btn btn-primary btn-lg" value="Zadnja kamera" onClick="backCamera()" id="back">
				<input type=button class="btn btn-danger btn-lg" value="Zaustavi kameru" onClick="test()" id="test">
				<input type=button class="btn btn-danger btn-lg" value="Napravi testnu sliku" onClick="stopCamera()" id="stop">
				<!--<input type=button class="btn btn-secondary btn-lg" value="Zaustavi kameru" onClick="stopCamera()" id="stop">
				<input type=button class="btn btn-secondary btn-lg" value="Zaustavi kameru" onClick="record()" id="stop">-->
                <button class="btn btn-success btn-lg" onclick="startRecording()" id="start">Pokreni snimanje</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
                <div id="my_camera" class="img-responsive"></div>
                <!--<input type=button value="Snimi sliku" onClick="take_snapshot()">-->
                <input type="hidden" name="image" class="image-tag">
            </div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div id="results"></div>
			</div>
		</div>
		<br>
    </form>
</div>
  
<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
function frontCamera(){
	stopCamera();
// Check browser support
if (typeof(Storage) !== "undefined") {
  // Store
  localStorage.setItem("camera", "front");
  // Retrieve
  document.getElementById("result").innerHTML = localStorage.getItem("camera");
} else {
  document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
}

	Webcam.set({
		constraints: {
				facingMode: 'user'
			},
        width: 640,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
	Webcam.attach('#my_camera');
}

function backCamera(){
	stopCamera();
	if (typeof(Storage) !== "undefined") {
  // Store
  localStorage.setItem("camera", "back");
  // Retrieve
  document.getElementById("result").innerHTML = localStorage.getItem("camera");
} else {
  document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
}
	Webcam.set({
		constraints: {
				facingMode: 'environment'
			},
        width: 640,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
	Webcam.attach( '#my_camera' );
}
function record(){
	
	var min = 5,
    max = 20;
	var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 10
	if(localStorage.getItem("camera")=="front"){
		frontCamera();
		timer=setTimeout(startRecording, rand * 1000);
	}
	else if(localStorage.getItem("camera")=="back"){
		backCamera();
		timer=setTimeout(startRecording, rand * 1000);
	}
	//setTimeout(startRecording, rand * 1000);
	//setInterval(startRecording, rand*1000);
}
 
function stopCamera(){
	Webcam.reset(); 
	clearTimeout(timer);
	
}
function test(){
	if (typeof(Storage) !== "undefined") {
		localStorage.setItem("test", "true");
		<?php session_start(); $_SESSION['test']="DEMO"?>
	}
}
function startRecording(){
	Webcam.snap( function(data_uri) {
	$(".image-tag").val(data_uri);
		document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
		} );
	document.getElementById("form").submit();
}

 
</script>
 
</body>
</html>